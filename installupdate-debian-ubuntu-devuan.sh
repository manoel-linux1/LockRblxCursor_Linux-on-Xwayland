#!/bin/bash

clear

if [[ $EUID -eq 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(This script should not be executed as a superuser or sudo)"
echo "(Please run it without superuser privileges or sudo)"
echo "#################################################################"
exit 1
fi

clear

sudo apt-get install --no-install-recommends xdotool python3-pynput xfwm4 weston

clear

echo "#################################################################"

sudo rm -rf /usr/bin/startfixmouse-roblox

sudo rm -rf /usr/bin/fixmouse.py

sudo rm -rf /usr/bin/settings.cfg

echo "#################################################################"

clear

echo "#################################################################"

sudo cp startfixmouse-roblox /usr/bin/

sudo cp fixmouse.py /usr/bin/

sudo cp settings.cfg /usr/bin/

echo "#################################################################"

clear

echo "#################################################################"

sudo chmod +x /usr/bin/startfixmouse-roblox

echo "#################################################################"

clear

cd $HOME

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ "  
echo "#################################################################"