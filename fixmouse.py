from pynput import mouse, keyboard
from pynput.mouse import Controller
import os
import re
import time
import subprocess

window_info = {
    "X": 0,
    "Y": 0,
    "WIDTH": 0,
    "HEIGHT": 0,
}

look_for = ["X", "Y", "WIDTH", "HEIGHT"]

settings = {
    "keybind": "<ctrl>+<shift>+o",
    "edge-offset": 1,
}

script_path = os.path.dirname(__file__)
config_path = os.path.join(script_path, "settings.cfg")
config_file = os.path.isfile(config_path)

if not config_file:
    print("Config file 'settings.cfg' not found!")
    print("Using default settings:")
    for key in settings.keys():
        print(key, settings[key])
else:
    with open(config_path) as config_file_object:
        for line in config_file_object.readlines():
            params = line.split("=")
            params[0] = params[0].strip()
            params[1] = params[1].strip()
            
            if params[0] in settings:
                try:
                    settings[params[0]] = int(params[1])
                except ValueError:
                    settings[params[0]] = params[1]

last_x_y = (0, 0)
pause = False
mouse_object = Controller()

print("Sleeping for 3 seconds...")
time.sleep(3)

def get_window_geometry():
    output = os.popen("xdotool getwindowfocus getwindowgeometry --shell")
    window_info = {
        "X": 0,
        "Y": 0,
        "WIDTH": 0,
        "HEIGHT": 0,
    }
    for line in output.readlines():
        for target in look_for:
            if not line.find(target) == -1:
                window_info[target] = int(re.findall("\d+", line)[0])
    return window_info

def move_mouse_to(x, y):
    try:
        subprocess.run(["xdotool", "mousemove", str(x), str(y)], check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: Failed to move mouse to ({x}, {y}) with xdotool: {e}")

def on_move(x, y):
    global window_info, pause
    if pause:
        return

    error_offset = settings["edge-offset"]
    error_offset_safe = error_offset + 1

    if x <= window_info["X"] + error_offset:
        move_mouse_to(window_info["X"] + window_info["WIDTH"] - error_offset_safe, y)
        return

    if x >= (window_info["X"] + window_info["WIDTH"]) - error_offset:
        move_mouse_to(window_info["X"] + error_offset_safe, y)
        return

    if y >= (window_info["Y"] + window_info["HEIGHT"]) - error_offset:
        move_mouse_to(x, window_info["Y"] + window_info["HEIGHT"] - error_offset_safe)
        return

    if y <= window_info["Y"] + error_offset:
        move_mouse_to(x, window_info["Y"] + error_offset_safe)
        return

def on_keybind():
    global pause
    pause = not pause
    print("Paused" if pause else "Resumed")

print("Exit by pressing CTRL+C")
print("Keybind to pause is:", settings["keybind"])

window_info = get_window_geometry()

hotkey_event_listener = keyboard.GlobalHotKeys({
    settings["keybind"]: on_keybind
})

hotkey_event_listener.start()

with mouse.Listener(on_move=on_move) as event:
    event.join()