# LockRblxCursor_Linux-on-Xwayland

NOTE: that the shell script named `startfixmouse-roblox` will close all open Python3 programs.

Python3 script to lock your cursor on waydroid sessions

This only works on Wayland

# Dependencies

- pynput
- xdotool
- xfwm4
- weston

# Install

If you have git installed, you can run:
`git clone https://gitlab.com/manoel-linux1/LockRblxCursor_Linux-on-Xwayland.git` 

to clone this git repository or download this as a zip.

cd `LockRblxCursor_Linux-on-Xwayland`

chmod a+x `installupdate-debian-ubuntu-devuan.sh`

./`installupdate-debian-ubuntu-devuan.sh`

# LockRblxCursor_Linux

After successful installation run the command `startfixmouse-roblox` just run Waydroid on Weston Roblox will have the mouse stuck.

# Configuration 

You can configure the script by editing the file, `/usr/bin/settings.cfg`

```
keybind=<ctrl>+<shift>+o
edge-offset=1
```

### keybind

You can change your keybind to pause the cursor locking.

For special keys (ctrl, shift and tab, for example) inclose them in <> brackets:

`<ctrl> or <shift> or <tab>`

and to add multiple keys together, add the plus sign in between:

``<ctrl>+a+b``

### edge-offset

You can change `edge-offset` to keep further keep the cursor enclosed.

( if for some reason the cursor doesn't wrap around the edges )

Higher `edge-offset` will keep the cursor more enclosed, but camera movements may become less smoother